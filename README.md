## Set up
*  clone the repo
*  paste the this code at the end of your .bashrc (or similar)


`for f in ~/.custom-shell-functions/*;
do
    source $f;
done
`

*  logout & login
*  




### Custom functions

* docker ps short format
    * displays only: **ID | Created | Status | Ports | Name** - (removes Image)
* cron complete removal prevention
    * displays a message instead of removing all jobs while tapping `crontab -r` which is dangerously close to `crontab -e`
