#!/bin/bash

crontab() {
    if [[ $1 == "-r" ]]; then
        command echo -e "I'm pretty sure you dont wanna do that\nIf you need to remove anything try one by one using crontab -e"
    else
        command crontab "$@"
    fi
}

