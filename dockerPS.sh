#!/bin/bash

docker() {
    if [[ $1 == "ps" ]]; then
        command docker "$@" --format 'table {{.ID}}\t{{.RunningFor}}\t{{.Status}}\t{{.Ports}}\t{{.Names}}\t{{.Size}}'
    else
        command docker "$@"
    fi
}

